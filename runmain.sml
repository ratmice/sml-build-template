
(* MLTon expects main to be run from top-level this should probably call exit *)
structure RunMain =
struct
  val _ = Main.main(CommandLine.name(), CommandLine.arguments())
end
