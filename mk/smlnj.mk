.PHONY: smlnj

smlnj: $(PROJECT)

$(PROJECT): $(PROJECT)-smlnj
	ln -sf $(PROJECT)-smlnj $(PROJECT)

$(PROJECT): $(PROJECT).$(HEAP_SUFFIX)
$(PROJECT)-smlnj: $(PROJECT).$(HEAP_SUFFIX)
$(PROJECT).$(HEAP_SUFFIX): $(CM_FILES)

%.$(HEAP_SUFFIX): %.cm
	(echo 'CM.make "$<";';echo 'SMLofNJ.exportFn("$@",Main.main);') | CM_VERBOSE=false sml

%-smlnj: %.$(HEAP_SUFFIX)
	(echo "#!/bin/sh";echo "sml @SMLload=$< \$$@") >$@
	chmod +x $@

$(foreach file,\
    $(CM_FILES),\
    $(eval $(PROJECT).$(HEAP_SUFFIX):$(shell $(SMLNJDEPS) -os $(OS) -arch $(ARCH) $(file))))
