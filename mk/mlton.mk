.PHONY: mlton 

mlton:$(PROJECT)
$(PROJECT): $(PROJECT)-mlton
$(PROJECT)-mlton: $(PROJECT).mlb

%-mlton: %.mlb
	$(MLTON) -output $@ $<

%: %.mlb
	ln -sf $(PROJECT)-mlton $(PROJECT)

$(foreach file,\
    $(CM_FILES),\
    $(eval $(PROJECT)-mlton: $(shell $(MLTON) -stop f $(file:.cm=.mlb))))

