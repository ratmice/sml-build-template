
OS :=\
$(shell echo \
  'TextIO.output (TextIO.stdErr, ((String.map (Char.toLower) (SMLofNJ.SysInfo.getOSName ())) ^ "\n"));' \
  | sml 2>&1 1> /dev/null \
)

ARCH :=\
$(shell echo \
  'TextIO.output (TextIO.stdErr, ((String.map (Char.toLower) (SMLofNJ.SysInfo.getTargetArch ())) ^ "\n"));' \
  | sml 2>&1 1> /dev/null \
)

HEAP_SUFFIX :=\
$(shell echo \
  'TextIO.output (TextIO.stdErr, (SMLofNJ.SysInfo.getHeapSuffix () ^ "\n"));' \
  | sml 2>&1 1> /dev/null \
)
SMLDIR := $(dir $(shell which sml))
