PROJECT	:=template
include mk/smlnjvars.mk
CM_FILES  :=$(PROJECT).cm
MLTON	  :=mlton
# Should be in .smackage/bin add that to path PATH
SMLNJDEPS :=$(shell which smlnjdeps)
export
.DEFAULT_GOAL:=smlnj
.PHONY: mlton smlnj

ifeq ($(MAKECMDGOALS),)
  GOAL=$(.DEFAULT_GOAL)
else
  GOAL=$(MAKECMDGOALS)
endif

smlnj:
	$(MAKE) -f mk/$(GOAL).mk $(GOAL)
mlton: $(PROJECT).mlb
	$(MAKE) -f mk/$(GOAL).mk $(GOAL)

%.mlb: %.cm 
	cm2mlb -DMLton $^ > $@ || (ret=$$?; $(RM) $@; exit $$ret)

clean:
	$(RM) $(PROJECT) $(PROJECT).mlb $(PROJECT)-mlton $(PROJECT)-smlnj $(PROJECT).$(HEAP_SUFFIX)
