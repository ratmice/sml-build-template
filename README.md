
this is a silly bunch of Makefiles for sml that i use,
All build information is controlled from SML of NJ's CM.
The makefiles require GNU make.

build goes through 2 phases
A) generating mlton mlb from the CM file.. with cm2mlb
B) automatic dependency generation from the mlb or cm 

mlton dependencies are generated from the mlton compiler.
smlnj dependencies are generated with smlnjdeps 

run 'make smlnj' to build a smlnj heap image and a shell script to run it.
run 'make mlton' to build a mlton executable.

the default target is smlnj

Required tools:
* https://gitlab.com/ratmice/smlnjdeps
* https://github.com/MLton/mlton/tree/master/util/cm2mlb
* https://www.gnu.org/software/make/
